package com.luv2code.springdemo;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class FileFortuneService implements FortuneService{

    private String path = "src/com/luv2code/data/FortuneList.txt";

    private List<String> theFortunes;

    // create a random number generator
    private Random myRandom = new Random();

    public FileFortuneService() {

        File theFile = new File(path);

        // initialize array list
        theFortunes = new ArrayList<String>();

        System.out.println("Reading fortunes from file: " + theFile);
        System.out.println("File exists: " + theFile.exists());

        try (
                BufferedReader bufferedReader = new BufferedReader( new FileReader(theFile))
        ) {
            String tempLine;

            while ((tempLine = bufferedReader.readLine()) != null){
                theFortunes.add(tempLine);
            }

        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String getFortune() {

        // Pick a random string from the array
        int index = myRandom.nextInt(theFortunes.size());

        return theFortunes.get(index);

    }
}

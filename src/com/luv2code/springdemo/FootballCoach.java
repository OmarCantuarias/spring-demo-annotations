package com.luv2code.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class FootballCoach implements Coach{

    private FortuneService fortuneService;

    // define a default constructor
    public FootballCoach(){
        System.out.println(">> FootballCoach: Inside default constructor");
    }


// Constructor Injection
    @Autowired
    public FootballCoach(@Qualifier("fileFortuneService") FortuneService theFortuneService){
        fortuneService = theFortuneService;
    }

    @Override
    public String getDailyWorkout() {
        return "Practice your penalty shots";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

}
